#ifndef SECONDSLEEP_H_
#define SECONDSLEEP_H_

#include <stdio.h>

#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h> // for nanosleep
#else
#include <unistd.h> // for usleep
#endif

#define SECOND_SLEEP (367)

void sleep_ms(int milliseconds);

#endif // SECONDSLEEP_H_
