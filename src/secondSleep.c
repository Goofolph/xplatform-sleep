#include "secondSleep.h"

void sleep_ms(int milliseconds) // cross-platform sleep function
{
#ifdef WIN32
  Sleep(milliseconds);
  printf("WIN32\n");
#elif _POSIX_C_SOURCE >= 199309L
  struct timespec ts;
  ts.tv_sec = milliseconds / 1000;
  ts.tv_nsec = (milliseconds % 1000) * 1000000;
  nanosleep(&ts, NULL);
  printf("_POSIX_C_SOURCE\n");
#else
  usleep(milliseconds * 1000);
  printf("ELSE\n");
#endif
}
