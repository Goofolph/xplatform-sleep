target = main

CC = gcc
CFLAGS = -g -Wall -Wextra
LD = $(CC)
LDFLAGS = -lsndfile -lopenal

RM = rm -f
MKDIR = mkdir -p

SRC = src
OBJ = obj
BIN = bin
SOURCES = $(wildcard $(SRC)/*.c)
OBJECTS = $(patsubst $(SRC)/%.c,$(OBJ)/%.o,$(SOURCES))

.PHONY: all
all: $(BIN)/$(target)

$(BIN)/$(target): $(OBJECTS)
	@$(MKDIR) $(@D)
	@printf "Linking $@\n"
	@$(LD) $(LDFLAGS) -o $@ $^

$(OBJ)/%.o: $(SRC)/%.c
	@$(MKDIR) $(@D)
	@printf "Compiling $@\n"
	@$(CC) $(CFLAGS) -o $@ -c $<

.PHONY: clean
clean:
	@printf "Cleaning object files\n"
	@$(RM) $(OBJECTS) $(OBJECTS:.o=.d)

.PHONY: remove
remove:
	@printf "Removing binaries\n"
	@$(RM) $(BIN)/$(target)

CFLAGS += -MMD
-include $(OBJECTS:.o=.d)
